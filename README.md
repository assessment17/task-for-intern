
# D&R Stajyer Görevlendirmesi / Backend Task

## Teknik Bilgiler

- PHP dilinde geliştirilmelidir.
- Laravel framework kullanılmalıdır. Eloquent kullanılmalıdır. 
- Kullanılacak teknolojilerde ürün kısıtı yoktur. Örn: Veritabanı olarak Mysql ya da PostgreSql kullanılabilir.
- Kullanılması gerektiği düşünülen yerlerde istenilen teknoloji kullanılabilir. Örn: Redis, RabbitMq, Memcahce, ElasticSearch vb.

## Görev Konusu

Aşağıdaki özelliklere sahip bir e-ticaret admin panel geliştirilmelidir:
    
1. Login ekranı
2. Admin Kullanıcı Yönetimi
3. Kategori Yönetimi
4. Ürün Yönetimi

> İlgili işlerin iş id'leri (Task ID) parantez içlerinde belirtilmiştir.

## Görev Teslimi ve Versiyonlama

Görev süresince geliştirilen kodlar git e (github, gitlab, bitbucket vs.) pushlanmalıdır. 
    - Her iş maddesine başlarken ilgili Task ID adı ile local branch açılmalıdır. 
    - Her iş üzerinde yapılan tüm çalışmalar, ilgili işin branch'ine (remote) pushlanmalıdır. 
    - Her günün sonunda mutlaka yapılan çalışmalar git branchine  pushlanmalıdır.
    - Tüm işlerin yer aldığı branch Master branch'dir. Tamamlanan işler Master branch'e Merge edilmelidir.

## Görev Detayları
- 1.Login ekranı (DR-1012)
    - Sistemde kayıtlı olan kullanıcıların login olabilmesi için Admin Panel Login ekranı geliştirilmelidir.

    
- 2.Admin Kullanıcı Yönetimi
    - Kullanıcı Ekleme formu (DR-1001)
        - Username (Kullanıcı Login), UserTitle (Kullanıcı Adı), Password (Kullanıc Şifre) alanlarından oluşmalıdır.
        - Daha önceden bu Username e sahip kullanıcının olup olmadığı kontrol edilmelidir. (Username -> Unique)
        - Şifre en az 6 karakterden oluşmalıdır.
        - Username alfanumeric karakterlerden oluşmalıdır, boşluk içermemelidir.
    -  Kullanıcı Listeleme Sayfası (DR-1002)
        - Tüm admin kullanıcılarının listelendiği ekran geliştirilmelidir.
        - Seçilen kullanıcılar toplu olarak silinebilmelidir.
        - Her kullanıcı satırında "Düzenle" isminde Kullanıcı Düzenle sayfasına yönlendirilecek bir buton yer almalıdır.
        - Her kullanıcı satırında "Sil" isminde Kullanıcı Silmesine sayfasına yönlendirilecek bir buton yer almalıdır.
    - Kullanıcı Düzenleme formu (DR-1003)
        - Ekleme formundaki ile aynı inputlar yer almalıdır
        - Yalnızca Username ve User title alanları veritabanından doldurulmuş olarak gösterilmelidir.
        - Şifre girilirse, şifre güncellenmelidir. Girilmezse, şifre değiştirilmemelidir.
        - Güncelleme anında Username unique olduğu için, yine kontrol yapılmalıdır.
    
    - Kullanıcı Silme (DR-1004)
        - Kullanıcı sil sayfasında kullanıcı Soft Delete silinmelidir.
        - Silme işleminden sonra sayfa tekrar Kullanıcı Listeleme sayfasına yönlendirilmelidir.
    
- 3.Kategori Yönetimi
    - Kategori Ekleme Formu (DR-1005)
        - CategoryTitle(Kategori Adı), CategoryDescription (Kategori Açıklaması) , Status(Kategori Durumu) inputlarından oluşmalıdır.
        - Kategori Adı uniqe'dir.
        - Alt/Üst kategori hiyerarşisi olmasına gerek yoktur.
    - Kategori Listeleme Sayfası (DR-1006)
        - Sistemde eklenen tüm kategorilerin listelendiği ekrandır.
        - Her kategori satırında "Düzenle" isminde Kategori Düzenle sayfasına yönlendirilecek bir buton yer almalıdır.
        - Her kategori satırında "Sil" isminde Kategori Sil sayfasına yönlendirilecek bir buton yer almalıdır.
    - Kategori Düzenleme Formu (DR-1007)
        - Ekleme formundaki ile aynı inputlar yer almalıdır
        - Tüm alanlar, veritabanından doldurulmuş olarak gösterilmelidir.
        - Güncelleme anında CategoryTitle unique olduğu için, yine kontrol yapılmalıdır.
    - Kategori Silme (DR-1008)
        - Kategori sil sayfasında kategori Soft Delete silinmelidir.
        - Silme işleminden sonra sayfa tekrar Kategori Listeleme sayfasına yönlendirilmelidir.,
        - Kategori silindiğinde, bu kategoriye ait tüm ürünlerin CategoryId alanları null 'a çekilmelidir
- 4.Ürün Yönetimi
    - Ürün Ekleme Formu (DR-1009)
        - ProductTitle(Ürün Adı), ProductCategoryId(Ürün Kategori ID'si), Barcode (Ürün Barkodu) ProductStatus(Ürün Durumu) inputlarından oluşmalıdır.
        - Kategori seçimi zorunlu değildir, diğer alanlar zorunludur.
        - Ürün ekleme formunda Kategori alanı menüden seçilebilmelidir.
    - Ürün Listeleme Sayfası (DR-1010)
        - Sistemde eklenen tüm ürünlerin listelendiği ekrandır.
        - Her ürün satırında "Düzenle" isminde Ürün Düzenle sayfasına yönlendirilecek bir buton yer almalıdır.
        - Her ürün satırında "Sil" isminde Ürün Silme sayfasına yönlendirilecek bir buton yer almalıdır.
    - Ürün Silme (DR-1011)
        - Ürün Sil sayfasında ürün Soft Delete silinmelidir.
        - Silme işleminden sonra sayfa tekrar Ürün Listeleme sayfasına yönlendirilmelidir.

## Dikkat Edilecek Hususlar

- Tüm Form submitlerinde validasyonlar yapılmalıdır ve hata durumunda hata mesajları dönülmelidir.
- Veritabanında tablolar migrationlar ile oluşturulmalıdır.
- Eloquent kullanılmalıdır. Performans durumları açısından kullanılmasının gerektiğini düşündüğünüz alanlarda QueryBuilder kullanabilirsiniz.
- Solid prensiplerine dikkat edilmelidir.
- Ana veriler (User, Category, Product vb.), proje kurulumu için seeder olarak sample data oluşturulmalıdır.

